import React, { FunctionComponent } from "react";
import { styled } from "@material-ui/core";
import { BridgeChainConfig, CurrencyConfig } from "../../../utils/assetConfigs";
import { HMSCountdown } from "../../transactions/components/TransactionsHelpers";

export const mintTooltips = {
  sending: "La cantidad y el activo que envía antes de que se apliquen las tarifas.",
  to: "La cadena de bloques a la que está enviando el activo.",
  recipientAddress: "La billetera que recibirá los activos acuñados.",
};

export const getMintDynamicTooltips = (
  chainConfig: BridgeChainConfig,
  chainNativeCurrencyConfig: CurrencyConfig
) => ({
  acknowledge: `Acuñando un activo en ${chainConfig.full} equiere que envíe una transacción. Te costará una pequeña cantidad de ${chainNativeCurrencyConfig.short}.`,
});

export const DepositWrapper = styled("div")({
  position: "relative",
});

type AddressValidityMessageProps = {
  milliseconds: number;
  destNetwork: string;
};

export const AddressValidityMessage: FunctionComponent<AddressValidityMessageProps> = ({
  milliseconds,
  destNetwork,
}) => {
  return (
    <span>
      Esta dirección de puerta de enlace vence en{" "}
      <HMSCountdown milliseconds={milliseconds} />. No envíe depósitos múltiples o depósitos después de que haya vencido. <br />
      Una vez que haya depositado fondos en la dirección de puerta de enlace, tiene 24 horas para enviar la transacción de menta a {destNetwork}
    </span>
  );
};

export const MultipleDepositsMessage: FunctionComponent = () => {
  return (
    <span>
      RenBridge ha detectado otro depósito en la misma dirección de puerta de enlace. Requerirá un envío adicional a la cadena de destino a través de su billetera web3.
    </span>
  );
};
