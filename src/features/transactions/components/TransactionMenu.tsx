import {
  Button,
  Divider,
  ListItemIcon,
  makeStyles,
  MenuItem,
  MenuItemProps,
  Typography,
} from "@material-ui/core";
import { GatewaySession } from "@renproject/ren-tx";
import classNames from "classnames";
import React, { FunctionComponent, useCallback, useState } from "react";
import {
  ActionButton,
  ActionButtonWrapper,
  RedButton,
} from "../../../components/buttons/Buttons";
import { CircleIcon } from "../../../components/icons/IconHelpers";
import {
  AddIcon,
  CustomSvgIconComponent,
  DeleteIcon,
  TxSettingsIcon,
} from "../../../components/icons/RenIcons";
import {
  OutlinedTextField,
  OutlinedTextFieldWrapper,
} from "../../../components/inputs/OutlinedTextField";
import { PaperContent } from "../../../components/layout/Paper";
import { externalLinkAttributes } from "../../../components/links/Links";
import {
  BridgeModalTitle,
  NestedDrawer,
  NestedDrawerActions,
  NestedDrawerContent,
  NestedDrawerWrapper,
} from "../../../components/modals/BridgeModal";

const useTransactionMenuItemStyles = makeStyles((theme) => ({
  root: {
    padding: "8px 20px",
    fontSize: 12,
  },
  iconWrapper: {
    minWidth: 32,
  },
}));

type TransactionMenuItemProps = MenuItemProps & {
  Icon: CustomSvgIconComponent;
};

export const TransactionMenuItem: FunctionComponent<TransactionMenuItemProps> = ({
  onClick,
  Icon,
  children,
  className,
  button,
  ...rest
}) => {
  const styles = useTransactionMenuItemStyles();
  return (
    <MenuItem
      dense
      onClick={onClick}
      className={classNames(styles.root, className)}
      {...rest}
    >
      <ListItemIcon className={styles.iconWrapper}>
        <CircleIcon Icon={Icon} fontSize="small" variant="outlined" />
      </ListItemIcon>
      <Typography variant="inherit">{children}</Typography>
    </MenuItem>
  );
};

const useTransactionMenuStyles = makeStyles((theme) => ({
  root: {
    fontSize: 12,
  },
  modalTitle: {
    padding: `12px 20px`,
  },
  titleIconWrapper: {
    minWidth: 32,
  },
  menuItems: {
    paddingTop: 6,
    minHeight: 150,
  },
  transferId: {
    paddingBottom: 10,
  },
}));

export type UpdateTxFn = (amount: number, vOut: number, txHash: string) => void;

type TransactionMenuProps = {
  open: boolean;
  onClose: () => void;
  onDeleteTx: () => void;
  onUpdateTx?: UpdateTxFn;
  tx: GatewaySession;
};

export const TransactionMenu: FunctionComponent<TransactionMenuProps> = ({
  open,
  onClose,
  onDeleteTx,
  onUpdateTx,
  tx,
}) => {
  const styles = useTransactionMenuStyles();
  const handleClose = useCallback(() => {
    if (onClose) {
      onClose();
    }
  }, [onClose]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const handleConfirmClose = useCallback(() => {
    setConfirmOpen(false);
  }, []);
  const handleDeleteWithConfirm = useCallback(() => {
    setConfirmOpen(true);
  }, []);

  const [updateOpen, setUpdateOpen] = useState(false);
  const handleUpdateClose = useCallback(() => {
    setUpdateOpen(false);
  }, []);
  const handleUpdateOpen = useCallback(() => {
    setUpdateOpen(true);
  }, []);

  return (
    <>
      <NestedDrawer open={open} onClose={handleClose} className={styles.root}>
        <BridgeModalTitle className={styles.modalTitle} onClose={handleClose}>
          <ListItemIcon className={styles.titleIconWrapper}>
            <CircleIcon
              Icon={TxSettingsIcon}
              fontSize="small"
              variant="solid"
            />
          </ListItemIcon>
          <Typography variant="inherit">Menú de transacciones</Typography>
        </BridgeModalTitle>
        <NestedDrawerWrapper>
          <NestedDrawerContent>
            <div className={styles.menuItems}>
              <TransactionMenuItem Icon={AddIcon} onClick={handleUpdateOpen}>
              Insertar / actualizar transacción
              </TransactionMenuItem>
              <TransactionMenuItem
                Icon={DeleteIcon}
                onClick={handleDeleteWithConfirm}
              >
                Eliminar transacción
              </TransactionMenuItem>
            </div>
          </NestedDrawerContent>
          <NestedDrawerActions>
            <PaperContent paddingVariant="medium" className={styles.transferId}>
              <Typography variant="inherit">ID de Transferencia: {tx.id}</Typography>
            </PaperContent>
            <Divider />
            <PaperContent bottomPadding topPadding paddingVariant="medium">
              <Button
                variant="outlined"
                size="small"
                href="https://renprotocol.typeform.com/to/YdmFyB"
                {...externalLinkAttributes}
              >
                Reportar un problema
              </Button>
            </PaperContent>
          </NestedDrawerActions>
        </NestedDrawerWrapper>
      </NestedDrawer>
      <ConfirmTransactionDeletionDrawer
        open={confirmOpen}
        onClose={handleConfirmClose as any}
        onDeleteTx={onDeleteTx}
      />
      {onUpdateTx && (
        <UpdateTransactionDrawer
          open={updateOpen}
          onClose={handleUpdateClose}
          onUpdateTx={onUpdateTx}
        />
      )}
    </>
  );
};

type ConfirmTransactionDeletionDrawerProps = {
  open: boolean;
  onClose: () => void;
  onDeleteTx: () => void;
};

export const ConfirmTransactionDeletionDrawer: FunctionComponent<ConfirmTransactionDeletionDrawerProps> = ({
  open,
  onClose,
  onDeleteTx,
}) => {
  const [deleting, setDeleting] = useState(false);
  const handleDeleteTx = useCallback(() => {
    setDeleting(true);
    onDeleteTx();
  }, [onDeleteTx]);
  return (
    <NestedDrawer title="Delete a Transaction" open={open} onClose={onClose}>
      <NestedDrawerWrapper>
        <NestedDrawerContent>
          <PaperContent topPadding>
            <Typography variant="h5" align="center" gutterBottom>
              Are you sure?
            </Typography>
            <Typography
              variant="body2"
              align="center"
              color="textSecondary"
              gutterBottom
            >
              Si ya ha enviado sus activos, los perderá para siempre si elimina la transacción.
            </Typography>
          </PaperContent>
        </NestedDrawerContent>
        <NestedDrawerActions>
          <PaperContent bottomPadding>
            <ActionButtonWrapper>
              <RedButton
                variant="text"
                color="inherit"
                startIcon={<DeleteIcon />}
                onClick={handleDeleteTx}
                disabled={deleting}
              >
                {deleting ? "Eliminando..." : "Eliminar"} Transacción
              </RedButton>
            </ActionButtonWrapper>
            <ActionButtonWrapper>
              <ActionButton onClick={onClose} disabled={deleting}>
              Cancelar
              </ActionButton>
            </ActionButtonWrapper>
          </PaperContent>
        </NestedDrawerActions>
      </NestedDrawerWrapper>
    </NestedDrawer>
  );
};

type UpdateTransactionDrawerProps = {
  open: boolean;
  onClose: () => void;
  onUpdateTx: UpdateTxFn;
};

const isValidInteger = (amount: string) => {
  return Number.isInteger(Number(amount));
};

export const UpdateTransactionDrawer: FunctionComponent<UpdateTransactionDrawerProps> = ({
  open,
  onClose,
  onUpdateTx,
}) => {
  const [amount, setAmount] = useState("");
  const [vout, setVout] = useState("");
  const [hash, setHash] = useState("");
  const [updating, setUpdating] = useState(false);
  const valid = amount && vout && hash;
  const handleAmountChange = useCallback((event) => {
    const newValue = event.target.value;
    if (isValidInteger(newValue)) {
      setAmount(newValue);
    }
  }, []);
  const handleVoutChange = useCallback((event) => {
    const newValue = event.target.value;
    if (isValidInteger(newValue)) {
      setVout(newValue);
    }
  }, []);
  const handleHashChange = useCallback((event) => {
    setHash(event.target.value);
  }, []);

  const handleUpdateTx = useCallback(() => {
    setUpdating(true);
    onUpdateTx(Number(amount), Number(vout), hash);
  }, [onUpdateTx, hash, vout, amount]);

  return (
    <NestedDrawer title="Actualizar el hash de transacción" open={open} onClose={onClose}>
      <NestedDrawerWrapper>
        <NestedDrawerContent>
          <PaperContent topPadding>
            <OutlinedTextFieldWrapper>
              <OutlinedTextField
                label="Monto (sats)"
                value={amount}
                onChange={handleAmountChange}
                placeholder="Ingrese la cantidad en sats"
              />
            </OutlinedTextFieldWrapper>
            <OutlinedTextFieldWrapper>
              <OutlinedTextField
                label="Hash de transacción"
                value={hash}
                onChange={handleHashChange}
                placeholder="Ingrese el hash de la transacción"
              />
            </OutlinedTextFieldWrapper>
            <OutlinedTextFieldWrapper>
              <OutlinedTextField
                label="vOut"
                value={vout}
                onChange={handleVoutChange}
                placeholder="Ingrese transacción vOut"
              />
            </OutlinedTextFieldWrapper>
          </PaperContent>
        </NestedDrawerContent>
        <NestedDrawerActions>
          <PaperContent bottomPadding>
            <ActionButtonWrapper>
              <RedButton
                variant="text"
                color="inherit"
                onClick={handleUpdateTx}
                disabled={updating || !valid}
              >
                {updating ? "Actualizando..." : "Actualizar"} transacción
              </RedButton>
            </ActionButtonWrapper>
            <ActionButtonWrapper>
              <ActionButton onClick={onClose} disabled={updating}>
                Cancel
              </ActionButton>
            </ActionButtonWrapper>
          </PaperContent>
        </NestedDrawerActions>
      </NestedDrawerWrapper>
    </NestedDrawer>
  );
};
