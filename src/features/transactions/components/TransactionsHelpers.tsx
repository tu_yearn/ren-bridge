import {
  Button,
  DialogProps,
  styled,
  Typography,
  useTheme,
} from '@material-ui/core'
import { GatewaySession } from '@renproject/ren-tx'
import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useState,
} from 'react'
import { useHistory } from 'react-router-dom'
import { useInterval } from 'react-use'
import {
  ActionButton,
  ActionButtonWrapper,
  RedButton,
} from '../../../components/buttons/Buttons'
import {
  SpecialAlertIcon,
  WarningIcon,
} from '../../../components/icons/RenIcons'
import {
  PaperContent,
  SpacedPaperContent,
} from '../../../components/layout/Paper'
import { Link } from '../../../components/links/Links'
import {
  BridgeModal,
  NestedDrawer,
  NestedDrawerActions,
  NestedDrawerContent,
  NestedDrawerWrapper,
} from '../../../components/modals/BridgeModal'
import {
  ProgressWithContent,
  ProgressWrapper,
  TransactionStatusInfo,
} from '../../../components/progress/ProgressHelpers'
import { links } from '../../../constants/constants'
import { paths } from '../../../pages/routes'
import { usePaperTitle } from '../../../providers/TitleProviders'
import { getFormattedHMS } from '../../../utils/dates'
import { trimAddress } from '../../../utils/strings'

export const ProcessingTimeWrapper = styled("div")({
  marginTop: 5,
  marginBottom: 5,
});

type BookmarkPageWarningProps = {
  onClosed?: () => void;
};

export const BookmarkPageWarning: FunctionComponent<BookmarkPageWarningProps> = ({
  onClosed,
}) => {
  const [open] = useState(true);
  const handleClose = useCallback(() => {
    if (onClosed) {
      onClosed();
    }
    // setOpen(false);
  }, [onClosed]);
  return (
    <NestedDrawer title="Warning" open={open} onClose={handleClose}>
      <NestedDrawerWrapper>
        <NestedDrawerContent>
          <PaperContent topPadding bottomPadding>
            <Typography variant="h5" align="center" gutterBottom>
            Marca esta pagina
            </Typography>
            <Typography variant="body2" align="center" gutterBottom>
            Para asegurarse de no perder el rastro de su transacción, marca esta pagina.
            </Typography>
          </PaperContent>
        </NestedDrawerContent>
        <NestedDrawerActions>
          <PaperContent bottomPadding>
            <ActionButtonWrapper>
              <ActionButton onClick={handleClose}>Entiendo</ActionButton>
            </ActionButtonWrapper>
          </PaperContent>
        </NestedDrawerActions>
      </NestedDrawerWrapper>
    </NestedDrawer>
  );
};

type ProgressStatusProps = {
  reason?: string;
  processing?: boolean;
};

export const ProgressStatus: FunctionComponent<ProgressStatusProps> = ({
  reason = "",
  processing = true,
}) => {
  const theme = useTheme();
  const [, setTitle] = usePaperTitle();
  useEffect(() => {
    setTitle(reason);
  }, [setTitle, reason]);
  return (
    <>
      <ProgressWrapper>
        <ProgressWithContent
          processing={processing}
          color={theme.palette.primary.main}
        >
          <TransactionStatusInfo status={reason} />
        </ProgressWithContent>
      </ProgressWrapper>
    </>
  );
};

export type TransactionItemProps = {
  tx: GatewaySession;
  isActive?: boolean;
  onContinue?: ((depositHash?: string) => void) | (() => void);
};

type HMSCountdownProps = { milliseconds: number };

export const HMSCountdown: FunctionComponent<HMSCountdownProps> = ({
  milliseconds,
}) => {
  const [count, setCount] = useState(milliseconds);
  useInterval(() => {
    setCount((ms) => ms - 1000);
  }, 1000);
  const time = getFormattedHMS(count);

  return <strong>{time}</strong>;
};

const ErrorIconWrapper = styled("div")(({ theme }) => ({
  fontSize: 72,
  lineHeight: 1,
  marginTop: 8,
  textAlign: "center",
  color: theme.customColors.textLight,
}));

type ErrorWithActionProps = DialogProps & {
  title?: string;
  onAction?: () => void;
  reason?: string;
  actionText?: string;
};

export const ErrorDialog: FunctionComponent<ErrorWithActionProps> = ({
  title = "Error",
  open,
  reason = "",
  actionText = "",
  onAction,
  children,
}) => {
  return (
    <BridgeModal open={open} title={title} maxWidth="xs">
      <SpacedPaperContent>
        <ErrorIconWrapper>
          <WarningIcon fontSize="inherit" color="inherit" />
        </ErrorIconWrapper>
        <Typography variant="h5" align="center" gutterBottom>
          {reason}
        </Typography>
        <Typography
          color="textSecondary"
          align="center"
          gutterBottom
          component="div"
        >
          {children}
        </Typography>
      </SpacedPaperContent>
      <PaperContent bottomPadding>
        <ActionButtonWrapper>
          <ActionButton onClick={onAction}>{actionText}</ActionButton>
        </ActionButtonWrapper>
      </PaperContent>
    </BridgeModal>
  );
};

export const SubmitErrorDialog: FunctionComponent<ErrorWithActionProps> = (
  props
) => (
  <ErrorDialog
    reason="Error submitting"
    actionText="Return to submission screen"
    {...props}
  >
    <span>Regrese a la pantalla anterior para volver a enviar</span>
  </ErrorDialog>
);

export const GeneralErrorDialog: FunctionComponent<ErrorWithActionProps> = (
  props
) => (
  <ErrorDialog
    reason="An error has occurred"
    actionText="Refresh page"
    {...props}
  >
    <span>
    Asegúrese de tener esta página marcada como favorita antes de actualizar. Si esto el error persiste, por favor{" "}
      <Link external href={links.BUGS_LOG} color="primary" underline="hover">
      enviar un error aquí
      </Link>
      .
    </span>
  </ErrorDialog>
);

export const ExpiredErrorDialog: FunctionComponent<ErrorWithActionProps> = (
  props
) => {
  const history = useHistory();
  const goToHome = useCallback(() => {
    history.push(paths.HOME);
  }, [history]);

  return (
    <ErrorDialog
      title="Expired"
      reason="This transaction has expired"
      actionText="Restart transaction"
      {...props}
    >
      <span>
      Las transacciones caducan después de 24 horas. Reinicie la transacción si quieres continuar.
      </span>
      <ActionButtonWrapper>
        <Button variant="text" color="inherit" onClick={goToHome}>
          Volver al inicio
        </Button>
      </ActionButtonWrapper>
    </ErrorDialog>
  );
};

type WarningWithActionsProps = DialogProps & {
  title?: string;
  reason?: string;
  onMainAction?: () => void;
  mainActionText?: string;
  mainActionDisabled?: boolean;
  onAlternativeAction?: () => void;
  alternativeActionText?: string;
  alternativeActionDisabled?: boolean;
};

export const WarningDialog: FunctionComponent<WarningWithActionsProps> = ({
  title = "Warning",
  open,
  reason = "",
  mainActionText = "",
  onMainAction,
  mainActionDisabled,
  alternativeActionText = "",
  onAlternativeAction,
  alternativeActionDisabled,
  children,
}) => {
  const showMainAction = onMainAction && mainActionText;
  return (
    <BridgeModal open={open} title={title} maxWidth="xs">
      <SpacedPaperContent>
        <ErrorIconWrapper>
          <SpecialAlertIcon fontSize="inherit" color="inherit" />
        </ErrorIconWrapper>
        <Typography variant="h5" align="center" gutterBottom>
          {reason}
        </Typography>
        <Typography
          color="textSecondary"
          align="center"
          gutterBottom
          component="div"
        >
          {children}
        </Typography>
      </SpacedPaperContent>
      <PaperContent bottomPadding>
        <ActionButtonWrapper>
          <RedButton
            variant="text"
            color="inherit"
            onClick={onAlternativeAction}
            disabled={alternativeActionDisabled}
          >
            {alternativeActionText}
          </RedButton>
        </ActionButtonWrapper>
        {showMainAction && (
          <ActionButtonWrapper>
            <ActionButton onClick={onMainAction} disabled={mainActionDisabled}>
              {mainActionText}
            </ActionButton>
          </ActionButtonWrapper>
        )}
      </PaperContent>
    </BridgeModal>
  );
};

type WrongAddressWarningDialog = WarningWithActionsProps & {
  address: string;
  addressExplorerLink: string;
  currency: string;
};

export const WrongAddressWarningDialog: FunctionComponent<WrongAddressWarningDialog> = ({
  address,
  addressExplorerLink,
  currency,
  ...props
}) => {
  return (
    <WarningDialog
      reason="Different account detected"
      alternativeActionText="Continue anyway"
      {...props}
    >
      <span>
      Esta transacción se creó con una cuenta diferente a la actual.
      cuenta (
        <Link
          external
          href={addressExplorerLink}
          color="primary"
          underline="hover"
        >
          {trimAddress(address, 5)}
        </Link>
        ). Si no tiene acceso a la cuenta que creó el transacción, no podrá acceder a la {currency}. Por favor
        cambia de cuenta en tu billetera.
      </span>
    </WarningDialog>
  );
};

export const AuthWarningDialog: FunctionComponent<WarningWithActionsProps> = ({
  ...props
}) => {
  return (
    <WarningDialog
      reason="Allow RenBridge to backup transactions"
      mainActionText="Sign & allow to continue"
      {...props}
    >
      <span>
      Para continuar, debe firmar con su billetera para permitir que RenBridge haga una copia de seguridad sus transacciones. Esto le permite reanudar una transacción si cierra su navegador web a mitad de la transacción.
      </span>
    </WarningDialog>
  );
};
