import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  Typography,
} from "@material-ui/core";
import React, { FunctionComponent, useCallback, useState } from "react";
import {
  ExpiredErrorDialog,
  GeneralErrorDialog,
  SubmitErrorDialog,
} from "../../../features/transactions/components/TransactionsHelpers";
import { ActionButton } from "../../buttons/Buttons";
import { Link } from "../../links/Links";
import { BridgeModal } from "../../modals/BridgeModal";
import { DotStepper } from "../../navigation/DotStepper";
import { RandomText, Section } from "../PresentationHelpers";

export const ModalsSection: FunctionComponent = () => {
  const [simpleOpened, setSimpleOpened] = useState(false);
  const [advancedOpened, setAdvancedOpened] = useState(false);
  const [activeStep, setActiveStep] = React.useState(0);
  const handleSimpleOpen = useCallback(() => {
    setSimpleOpened(true);
  }, []);
  const handleSimpleClose = useCallback(() => {
    setSimpleOpened(false);
  }, []);
  const handleAdvancedOpen = useCallback(() => {
    setAdvancedOpened(true);
  }, []);
  const handleAdvancedClose = useCallback(() => {
    setAdvancedOpened(false);
  }, []);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };
  return (
    <Section header="Modals">
      <Button variant="text" onClick={handleSimpleOpen}>
      Sencillo
      </Button>
      <Button variant="text" onClick={handleAdvancedOpen}>
      Paso a paso
      </Button>
      <BridgeModal
        open={simpleOpened}
        title="Advertencia"
        onClose={handleSimpleClose}
      >
        <DialogContent>
          <Typography variant="h5" align="center" gutterBottom>
          Soporte de billetera limitado
          </Typography>
          <Typography variant="body2" align="center" gutterBottom>
          RenBridge solo se ha probado y confirmado que funciona con algunas carteras WalletConnect.
          </Typography>
          <Typography variant="body2" align="center" gutterBottom>
          Si continúa con una billetera que no ha sido probada, corre el riesgo de perder fondos.
          </Typography>
          <Typography variant="body2" align="center">
            <Link href="/" color="primary" align="center">
            Ver la lista completa antes de continuar ↗
            </Link>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            size="large"
            onClick={handleSimpleClose}
            fullWidth
          >
            Continuar con WalletConnect
          </Button>
        </DialogActions>
      </BridgeModal>
      <BridgeModal
        open={advancedOpened}
        title="Info"
        onClose={handleAdvancedClose}
      >
        <DialogContent>
          {activeStep === 0 && (
            <>
              <Typography variant="h5" gutterBottom>
              No cierre esta pestaña del navegador
              </Typography>
              <Typography variant="subtitle2" gutterBottom>
                <RandomText words={5} />
              </Typography>
              <Box mt={3}>
                <ActionButton onClick={handleNext}>Siguiente</ActionButton>
              </Box>
            </>
          )}
          {activeStep === 1 && (
            <>
              <Typography variant="h5" gutterBottom>
                <RandomText words={1} />
              </Typography>
              <Typography variant="subtitle2" gutterBottom>
                <RandomText words={3} />
              </Typography>
              <Box mt={3}>
                <ActionButton onClick={handleAdvancedClose}>OK</ActionButton>
              </Box>
            </>
          )}
        </DialogContent>
        <DotStepper
          handleNext={handleNext}
          handleBack={handleBack}
          steps={2}
          activeStep={activeStep}
        />
      </BridgeModal>
      <SubmitErrorDialog open={false} onAction={() => {}} />
      <GeneralErrorDialog open={false} onAction={() => {}} />
      <ExpiredErrorDialog open={false} onAction={() => {}} />
    </Section>
  );
};
