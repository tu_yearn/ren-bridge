import { Box, Divider, Typography } from "@material-ui/core";
import React, { FunctionComponent } from "react";
import { BitcoinInCircleIcon } from "../../icons/RenIcons";
import { BridgePaper } from "../../layout/Paper";
import {
  AssetInfo,
  LabelWithValue,
  MiddleEllipsisText,
} from "../../typography/TypographyHelpers";
import { Section } from "../PresentationHelpers";

export const TypographyHelpersSection: FunctionComponent = () => {
  const address = "0x13123131241241241241212312314124124123412414124141000000";
  return (
    <Section header="Typography Helpers">
      <BridgePaper>
        <Box pt={3} />
        <Typography variant="body1" gutterBottom>
          Details
        </Typography>
        <LabelWithValue
          label="Enviando"
          value="1.0 BTC"
          valueEquivalent="10,131.65 USD"
        />
        <LabelWithValue label="A" value="Ethereum" />
        <LabelWithValue
          label="Dirección"
          value={<MiddleEllipsisText>{address}</MiddleEllipsisText>}
        />
        <LabelWithValue
          label="Dirección del receptor"
          value={<MiddleEllipsisText hoverable>{address}</MiddleEllipsisText>}
        />
        <LabelWithValue
          label="Dirección del receptor"
          value={<MiddleEllipsisText>{address}</MiddleEllipsisText>}
        />
        <Box mb={1}>
          <Divider />
        </Box>
        <Typography variant="body1" gutterBottom>
          Tarifas
        </Typography>
        <LabelWithValue
          label="Tarifa RenVM"
          labelTooltip="Explicación de la tarifa RenVM"
          value="0.10%"
          valueEquivalent="$11.80"
        />
        <LabelWithValue
          label="Bitcoin Miner Fee"
          labelTooltip="Explicando la tarifa de Bitcoin Miner"
          value="0.0007 BTC"
          valueEquivalent="$8.26"
        />
        <LabelWithValue
          label="Esti. Ethereum Fee"
          labelTooltip="Explicando Esti. Tarifa de Ethereum"
          value="200 GWEI"
          valueEquivalent="$6.42"
        />
        <Divider />
        <Box pt={2} />
        <AssetInfo
          label="Recibiendo:"
          value="0.31256113 BTC"
          valueEquivalent=" = $3,612.80 USD"
          Icon={<BitcoinInCircleIcon fontSize="inherit" />}
        />
      </BridgePaper>
    </Section>
  );
};
