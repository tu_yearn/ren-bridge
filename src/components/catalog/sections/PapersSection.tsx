import { Box, Button, Fade, Grow, IconButton, Typography, Zoom, } from '@material-ui/core'
import React, { FunctionComponent, useCallback, useState } from 'react'
import { ToggleIconButton, TransactionDetailsButton, } from '../../buttons/Buttons'
import { BackArrowIcon } from '../../icons/RenIcons'
import { BridgePurePaper, PaperActions, PaperContent, PaperHeader, PaperNav, PaperTitle, } from '../../layout/Paper'
import { Link } from '../../links/Links'
import { Section } from '../PresentationHelpers'

const SLIDES = 3;
export const ExampleContent: FunctionComponent = () => {
  return (
    <PaperContent bottomPadding>
      <Typography variant="h5" align="center" gutterBottom>
      Soporte de billetera limitado
      </Typography>
      <Typography variant="body2" align="center" gutterBottom>
      RenBridge solo se ha probado y se ha confirmado que funciona con algunos WalletConnect carteras.
      </Typography>
      <Typography variant="body2" align="center" gutterBottom>
      Si continúa con una billetera que no ha sido probada, está arriesgando pérdida de fondos.
      </Typography>
      <Typography variant="body2" align="center">
        <Link href="/" color="primary" align="center">
        Ver la lista completa antes de continuar ↗
        </Link>
      </Typography>
    </PaperContent>
  );
};

const CurrencyReceivedContent: FunctionComponent = () => {
  return (
    <PaperContent bottomPadding>
      <Button variant="contained" color="primary" size="large" fullWidth>
        Return
      </Button>
      <Box display="flex" justifyContent="space-between" py={2}>
        <Link external color="primary" variant="button" underline="hover">
          Ethereum transaction
        </Link>
        <Link external color="primary" variant="button" underline="hover">
          Bitcoin transaction
        </Link>
      </Box>
      <TransactionDetailsButton
        label="BNC"
        address="0x7a36479806342F7a1d663fF43A0D340b733FA764"
      />
    </PaperContent>
  );
};

export const PapersSection: FunctionComponent = () => {
  const [current, setCurrent] = useState(2);
  const [settings, setSettings] = useState(false);
  const [notifications, setNotifications] = useState(false);
  const handleNext = useCallback(() => {
    setCurrent((current) => (current + 1) % SLIDES);
  }, []);
  const handlePrev = useCallback(() => {
    setCurrent((curr) => (curr > 0 ? curr - 1 : SLIDES - 1));
  }, []);
  const toggleSettings = useCallback(() => {
    setSettings(!settings);
  }, [settings]);
  const toggleNotifications = useCallback(() => {
    setNotifications(!notifications);
  }, [notifications]);

  return (
    <Section header="Paper variants">
      <Button onClick={handleNext}>Siguiente</Button>
      {current === 0 && (
        <Zoom in={current === 0}>
          <BridgePurePaper>
            <PaperHeader>
              <PaperNav />
              <PaperTitle>Tarifas</PaperTitle>
              <PaperActions>
                <ToggleIconButton
                  variant="settings"
                  pressed={settings}
                  onClick={toggleSettings}
                />
                <ToggleIconButton
                  variant="notifications"
                  pressed={notifications}
                  onClick={toggleNotifications}
                />
              </PaperActions>
            </PaperHeader>
            <ExampleContent />
          </BridgePurePaper>
        </Zoom>
      )}
      {current === 1 && (
        <Fade in={current === 1}>
          <BridgePurePaper>
            <PaperHeader>
              <PaperNav>
                <IconButton onClick={handlePrev}>
                  <BackArrowIcon />
                </IconButton>
              </PaperNav>
              <PaperTitle>Tarifas</PaperTitle>
              <PaperActions>
                <ToggleIconButton
                  variant="settings"
                  pressed={settings}
                  onClick={toggleSettings}
                />
                <ToggleIconButton
                  variant="notifications"
                  pressed={notifications}
                  onClick={toggleNotifications}
                />
              </PaperActions>
            </PaperHeader>
            <ExampleContent />
          </BridgePurePaper>
        </Fade>
      )}
      {current === 2 && (
        <Grow in={current === 2}>
          <BridgePurePaper>
            <PaperHeader>
              <PaperNav />
              <PaperTitle />
              <PaperActions />
            </PaperHeader>
            <CurrencyReceivedContent />
          </BridgePurePaper>
        </Grow>
      )}
    </Section>
  );
};
